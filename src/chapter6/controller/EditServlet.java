package chapter6.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;

import chapter6.beans.Message;
import chapter6.exception.NoRowsUpdatedRuntimeException;
import chapter6.service.MessageService;

@WebServlet(urlPatterns = { "/edit" })
public class EditServlet extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
		throws IOException, ServletException {

		List<String> errorMessages = new ArrayList<String>();

		String stringMessageId = request.getParameter("message_id");
		Integer messageId = null;
		Message message = null;

		if(!StringUtils.isEmpty(stringMessageId) && stringMessageId.matches("^[0-9]+$")) {
			messageId = Integer.parseInt(stringMessageId);
			message = new MessageService().select(messageId);
		}

		if(message == null) {
			errorMessages.add("不正なパラメータが入力されました");
			request.setAttribute("errorMessages", errorMessages);
			request.getRequestDispatcher("./").forward(request, response);
			return;
		}

		request.setAttribute("message", message);
		request.getRequestDispatcher("edit.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
		throws IOException, ServletException {

		List<String> errorMessages = new ArrayList<String>();

		Message message = getMessage(request);
		String text = message.getText();

		if(!isValid(text, errorMessages)) {
			request.setAttribute("errorMessages", errorMessages);
			request.setAttribute("message", message);
			request.getRequestDispatcher("edit.jsp").forward(request, response);
			return;
		}

		if(isValid(text, errorMessages)) {
			try {
				new MessageService().update(message);
			} catch (NoRowsUpdatedRuntimeException e) {
                errorMessages.add("既に更新されています。最新のデータを表示しました。データを確認してください。");
            }
		}

		response.sendRedirect("./");
	}

	private Message getMessage(HttpServletRequest request) throws IOException, ServletException {

		Message message = new Message();
        message.setId(Integer.parseInt(request.getParameter("message_id")));
        message.setText(request.getParameter("text"));
        return message;
	}

	private boolean isValid(String text, List<String> errorMessages) {

		if(StringUtils.isBlank(text)) {
			errorMessages.add("メッセージを入力してください");
		} else if(140 < text.length()) {
			errorMessages.add("140文字以下で入力してください");
		}

		if(errorMessages.size() != 0) {
			return false;
		}
		return true;
	}
}
